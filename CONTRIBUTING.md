# Contributing to this project
You can contribute to this project.

## Commits
Use next tags at the beggining of each commit:

|  Tag   | Description              |
| :---:  | :---                     |
|[INIT]  | Initial commit           |
|[NEW]   | Add new file             |
|[DEL]   | Delete file              |
|[EDIT]  | Edit file                |
|[UPDATE]| Update project structure |
|[FIX]   | Fix bug                  |
|[TYPO]  | Fix typographic error    |
