<?php

namespace App\Entity;

use App\Repository\IOTAlarmsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IOTAlarmsRepository::class)
 */
class IOTAlarms
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IOTTriggers", inversedBy="id")
     */
    private $trigger;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IOTMeasurements", mappedBy="id")
     */
    private $measurements;

    public function __construct()
    {
        $this->measurements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getTrigger(): ?IOTTriggers
    {
        return $this->trigger;
    }

    public function setTrigger(?IOTTriggers $trigger): self
    {
        $this->trigger = $trigger;

        return $this;
    }

    /**
     * @return Collection|IOTMeasurements[]
     */
    public function getMeasurements(): Collection
    {
        return $this->measurements;
    }

    public function addMeasurement(IOTMeasurements $measurement): self
    {
        if (!$this->measurements->contains($measurement)) {
            $this->measurements[] = $measurement;
            $measurement->setId($this);
        }

        return $this;
    }

    public function removeMeasurement(IOTMeasurements $measurement): self
    {
        if ($this->measurements->removeElement($measurement)) {
            // set the owning side to null (unless already changed)
            if ($measurement->getId() === $this) {
                $measurement->setId(null);
            }
        }

        return $this;
    }
}
