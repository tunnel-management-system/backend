<?php

namespace App\Entity;

use App\Repository\IOTTriggersRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IOTTriggersRepository::class)
 */
class IOTTriggers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $trigger;

    /**
     * @ORM\Column(type="string")
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }
}
