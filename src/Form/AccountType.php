<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use App\Entity\User;

class AccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class)
            ->add('roles', ChoiceType::class, [
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Admin' => 'ROLE_ADMIN',
                    'Super Admin' => 'ROLE_SUPER_ADMIN'
                ]
            ])
            ->add('save', SubmitType::class)
        ;

        if ($options['display_all'] === true) {

            $builder
                ->add('name', TextType::class)
                ->add('surname', TextType::class)
                ->add('email', TextType::class)
                ->add('position', ChoiceType::class, [
                    'multiple' => false,
                    'expanded' => false,
                    'choices' => [
                        'Operator' => 'OPERATOR',
                        'Security' => 'SECURITY',
                        'Maintenance' => 'MAINTENANCE',
                        'Control office' => 'CONTROL_OFFICE',
                        'General office' => 'GENERAL_OFFICE',
                        'Visitor' => 'VISITOR'
                    ]
                ])
            ;

        }

        $builder->get('roles')
            ->addModelTransformer( new CallbackTransformer(
                function ($rolesAsArray) {
                    // transform an array to text
                    return count($rolesAsArray) ? $rolesAsArray[0] : null;
                },
                function ($rolesAsString) {
                    // transforms string to array
                    return [$rolesAsString];
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'display_all' => true
        ]);
    }
}