<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Doctrine\Persistence\ManagerRegistry;
use App\Form\AccountType;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login/index.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(): void
    {
        
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile(Request $request, ManagerRegistry $doctrine): Response
    {
        $user = $this->getUser();

        if ($user != null) {
            $form = $this->createForm(AccountType::class, $user);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();

                $entityManager = $doctrine->getManager();
                $entityManager->flush();

                return $this->redirectToRoute('profile');
            }

            return $this->renderForm('accounts/edit.html.twig', [
                'form' => $form,
            ]);
            
        } else {
            return $this->redirectToRoute('index');
        }
    }

}
