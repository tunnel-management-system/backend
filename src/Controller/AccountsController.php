<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;
use App\Form\AccountType;


/**
 * @IsGranted("ROLE_SUPER_ADMIN")
 * @Route("/accounts", name="accounts_")
 */
class AccountsController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ManagerRegistry $doctrine, UserPasswordHasherInterface $passwordHasher): Response
    {
        $users = $doctrine->getRepository(User::class)->findAll();

        $form = $this->createForm(AccountType::class, new User(), ['display_all' => false]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $plaintextPassword = "tunnel123";
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );
            $user->setPassword($hashedPassword);

            $entityManager = $doctrine->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('accounts_index');
        }

        return $this->render('accounts/index.html.twig', [
            'users' => $users,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request, ManagerRegistry $doctrine, UserPasswordHasherInterface $passwordHasher): Response
    {
        $user = new User();

        $form = $this->createForm(AccountType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $plaintextPassword = "tunnel123";
            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );
            $user->setPassword($hashedPassword);

            $entityManager = $doctrine->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('accounts_index');
        }

        return $this->renderForm('accounts/new.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(User $user, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $entityManager->remove($user);
        $entityManager->flush();

        $this->addFlash(
            'success',
            "User deleted successfully."
        );

        return $this->redirectToRoute('accounts_index');
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(User $user, Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(AccountType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $entityManager = $doctrine->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('accounts_index');
        }

        return $this->renderForm('accounts/edit.html.twig', [
            'form' => $form,
        ]);
    }
}
