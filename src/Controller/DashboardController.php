<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\IOTMeasurements;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/dashboard", name="dashboard_")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $val = [];
        $val['temperature'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getTemperature();
        $val['humidity'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getHumidity();
        $val['co'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getCO();
        $val['gas'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getGas();
        $val['potentiometer'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getPotentiometer();
        $val['htemperature'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getHTemperature();

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
            'data' => $this->getDoctrine()->getRepository(IOTMeasurements::class)->findBy([], ['id' => 'DESC'], 50),
            'val' => $val,
        ]);
    }

    /**
     * @Route("/graphics", name="graphics")
     */
    public function graphics(): Response
    {
        return $this->render('dashboard/graphics.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }

    /**
     * @Route("/alerts", name="alerts")
     */
    public function alerts(): Response
    {
        return $this->render('dashboard/alerts.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }

    /**
     * @Route("/actions", name="actions")
     */
    public function actions(): Response
    {
        return $this->render('dashboard/actions.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }
}
