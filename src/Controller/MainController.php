<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\IOTMeasurements;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $data = [];
        $data['message'] = "Tunnel available";
        $data['humidity'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getHumidity();
        $data['temperature'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getTemperature();
        $data['co'] = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getCO();

        return $this->render('main/index.html.twig', [
            'data' => $data,
        ]);
    }

    /**
     * @Route("/about_us", name="about")
     */
    public function about(): Response
    {
        return $this->render('main/about.html.twig');
    }
}
