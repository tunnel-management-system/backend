<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\IOTMeasurements;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/api", name="api_")
 */
class APIController extends AbstractController
{
    /**
     * @Route("/{val}", name="index")
     */
    public function getSeries(string $val)
    {
        if ($val == "temperature") {
            $data = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getTemperatureSeries();
        } else if ($val == "humidity") {
            $data = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getHumiditySeries();
        } else {
            $data = $this->getDoctrine()->getRepository(IOTMeasurements::class)->getPotentiometerSeries();
        }

        return new JsonResponse($data);
    }
}