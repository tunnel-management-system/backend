<?php

namespace App\MQTTMessenger;

use Symfony\Component\Messenger\Transport\TransportInterface;
use Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface;
use Symfony\Component\Messenger\Transport\Sender\SenderInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Envelope;
use App\MQTTMessenger\MQTTMessage;
use PhpMqtt\Client\MqttClient;
use App\Entity\IOTMeasurements;
use Doctrine\ORM\EntityManagerInterface;


class MQTTTransport implements TransportInterface
{
    private $MQTTServer;
    private $MQTTPort;
    private $MQTTClientId;

    private $MQTTUser;
    private $MQTTPass;

    private $MQTTInTopics;

    private $serializer;

    private $MQTTClient;
    private $isConnected;

    private $em;

    public function __construct(string $MQTTServer, int $MQTTPort, string $MQTTClientId, ?string $MQTTUser = null, ?string $MQTTPass = null, ?array $MQTTInTopics, ?SerializerInterface $serializer = null, ?EntityManagerInterface $em = null)
    {
        $this->MQTTServer = $MQTTServer;
        $this->MQTTPort = $MQTTPort;
        $this->MQTTClientId = $MQTTClientId;

        $this->MQTTUser = $MQTTUser;
        $this->MQTTPass = $MQTTPass;

        $this->MQTTInTopics = $MQTTInTopics;

        $this->serializer = $serializer;

        $this->MQTTClient = new MqttClient($MQTTServer, $MQTTPort, $MQTTClientId);
        $this->isConnected = false;

        $this->em = $em;
    }

    public function get(): iterable
    {
        $this->MQTTClient->connect();

        foreach ($this->MQTTInTopics as $topic) {
            $this->MQTTClient->subscribe($topic, function ($topic, $message) {
                echo sprintf("Received message on topic [%s]: %s\n", $topic, $message);

                $msg = new IOTMeasurements();
                $msg->setTopic($topic);
                $msg->setData($message);

                $this->em->persist($msg);
                $this->em->flush();
            }, 0);
        }

        $this->MQTTClient->loop(true);
        $this->MQTTClient->disconnect();
    }

    public function ack(Envelope $envelope): void
    {
        return;
    }

    public function reject(Envelope $envelope): void
    {
        return;
    }

    public function send(Envelope $envelope): Envelope
    {
        if ($envelope->getMessage() instanceof MQTTMessage) {
            $msg = $envelope->getMessage();

            $this->MQTTClient->connect();
            $this->MQTTClient->publish($msg->getTopic(), $msg->getContent());
            $this->MQTTClient->disconnect();
        }

        return $envelope;
    }
}