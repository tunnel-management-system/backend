<?php

namespace App\MQTTMessenger;


class MQTTMessage
{
    private $topic;
    private $content;
    private $qos;

    public function __construct(string $topic, string $content, int $qos = 0)
    {
        $this->topic = $topic;
        $this->content = $content;
        $this->qos = $qos;
    }

    public function getTopic(): string
    {
        return $this->topic;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getQos(): int
    {
        return $this->qos;
    }
    
}