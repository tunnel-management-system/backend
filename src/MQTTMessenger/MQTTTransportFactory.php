<?php

namespace App\MQTTMessenger;

use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use App\MQTTMessenger\MQTTTransport;
use Doctrine\ORM\EntityManagerInterface;


class MQTTTransportFactory implements TransportFactoryInterface
{
    private $MQTTInTopics;
    private $MQTTClientId;

    private $em;

    public function __construct(EntityManagerInterface $em, array $MQTTInTopics, ?string $MQTTClientId = null)
    {
        $this->MQTTInTopics = $MQTTInTopics;
        $this->MQTTClientId = $MQTTClientId;

        $this->em = $em;
    }

    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        if (false === $parsedURL = parse_url($dsn)) {
            throw new Exception("Invalid DSN provided.");
        }

        $MQTTServer = $parsedURL['host'] ?? 'localhost';
        $MQTTPort = $parsedURL['port'] ?? 1883;
        $MQTTClientId = $this->MQTTClientId ?? getmypid();

        $MQTTUser = isset($parsedURL["user"]) ? $parsedURL["user"] : null;
        $MQTTPass = isset($parsedURL["pass"]) ? $parsedURL["pass"] : null;

        return new MQTTTransport($MQTTServer, $MQTTPort, $MQTTClientId, $MQTTUser, $MQTTPass, $this->MQTTInTopics, $serializer, $this->em);
    }

    public function supports(string $dsn, array $options): bool
    {
        return 0 == strpos($dsn, "mqtt://");
    }
}