<?php

namespace App\MQTTMessenger;

use App\MQTTMessenger\MQTTMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use PhpMqtt\Client\MqttClient;


class MQTTMessageHandler implements MessageHandlerInterface
{
    private $MQTTServer;
    private $MQTTPort;
    private $MQTTClientId;

    public function __construct(string $MQTTServer, int $MQTTPort, ?string $MQTTClientId = null)
    {
        $this->MQTTServer = $MQTTServer;
        $this->MQTTPort = $MQTTPort;
        $this->MQTTClientId = $MQTTClientId ?? getmypid();
    }

    public function __invoke(MQTTMessage $message)
    {
        // Send message to MQTT broker
        $mqtt = new MqttClient($this->MQTTServer, $this->MQTTPort, $this->MQTTClientId);
        $mqtt->connect();
        $mqtt->publish($message->getTopic(), $message->getContent(), $message->getQos());
        $mqtt->disconnect();
    }
}