<?php

namespace App\Repository;

use App\Entity\IOTAlarms;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IOTAlarms|null find($id, $lockMode = null, $lockVersion = null)
 * @method IOTAlarms|null findOneBy(array $criteria, array $orderBy = null)
 * @method IOTAlarms[]    findAll()
 * @method IOTAlarms[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IOTAlarmsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IOTAlarms::class);
    }

    // /**
    //  * @return IOTAlarms[] Returns an array of IOTAlarms objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IOTAlarms
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
