<?php

namespace App\Repository;

use App\Entity\IOTTriggers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IOTTriggers|null find($id, $lockMode = null, $lockVersion = null)
 * @method IOTTriggers|null findOneBy(array $criteria, array $orderBy = null)
 * @method IOTTriggers[]    findAll()
 * @method IOTTriggers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IOTTriggersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IOTTriggers::class);
    }

    // /**
    //  * @return IOTTriggers[] Returns an array of IOTTriggers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IOTTriggers
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
