<?php

namespace App\Repository;

use App\Entity\IOTMeasurements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IOTMeasurements|null find($id, $lockMode = null, $lockVersion = null)
 * @method IOTMeasurements|null findOneBy(array $criteria, array $orderBy = null)
 * @method IOTMeasurements[]    findAll()
 * @method IOTMeasurements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IOTMeasurementsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IOTMeasurements::class);
    }


    public function getPotentiometer()
    {
        return $this->findOneBy(['topic' => 'CENTER/1/SECTION/1/S0001'], ['id' => 'DESC'])->getData();
    }

    public function getPotentiometerSeries()
    {
        $data = $this->findBy(['topic' => 'CENTER/1/SECTION/1/S0004'], ['id' => 'DESC'], 50);
        $d = [];
        foreach($data as $v) {
            array_push($d, floatval($v->getData()));
        }

        return $d;
    }


    public function getTemperature()
    {
        return $this->findOneBy(['topic' => 'CENTER/1/SECTION/1/S0002'], ['id' => 'DESC'])->getData();
    }

    public function getTemperatureSeries()
    {
        $data = $this->findBy(['topic' => 'CENTER/1/SECTION/1/S0002'], ['id' => 'DESC'], 50);
        $d = [];
        foreach($data as $v) {
            array_push($d, floatval($v->getData()));
        }

        return $d;
    }

    public function getHumidity()
    {
        return $this->findOneBy(['topic' => 'CENTER/1/SECTION/1/S0003'], ['id' => 'DESC'])->getData();
    }

    public function getHumiditySeries()
    {
        $data = $this->findBy(['topic' => 'CENTER/1/SECTION/1/S0003'], ['id' => 'DESC'], 50);
        $d = [];
        foreach($data as $v) {
            array_push($d, floatval($v->getData()));
        }

        return $d;
    }

    public function getGas()
    {
        return $this->findOneBy(['topic' => 'CENTER/1/SECTION/1/S0004'], ['id' => 'DESC'])->getData();
    }

    public function getCO()
    {
        return $this->findOneBy(['topic' => 'CENTER/1/SECTION/1/S0005'], ['id' => 'DESC'])->getData();
    }

    public function getHTemperature()
    {
        return $this->findOneBy(['topic' => 'CENTER/1/SECTION/1/S0006'], ['id' => 'DESC'])->getData();
    }

    public function getPIR()
    {
        return $this->findOneBy(['topic' => 'CENTER/1/SECTION/1/S0007'], ['id' => 'DESC'])->getData();
    }

    // /**
    //  * @return IOTMeasurements[] Returns an array of IOTMeasurements objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IOTMeasurements
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
